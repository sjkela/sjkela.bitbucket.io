// Select DOM items
const menuBtn = document.querySelector(".menu-btn");
const menu = document.querySelector(".menu");
const menuBrand = document.querySelector(".menu-branding");
const menuNav = document.querySelector(".menu-nav");
const navItems = document.querySelectorAll(".nav-item");

// Set initial stat of menuy

let showMenu = false;

menuBtn.addEventListener("click", toggleMenu);

function toggleMenu() {
  if (!showMenu) {
    menuBtn.classList.add("close");
    menu.classList.add("show");
    menuNav.classList.add("show");
    menuBrand.classList.add("show");
    navItems.forEach((element) => {
      element.classList.add("show");
    });

    // set menu state
    showMenu = true;
  } else {
    menuBtn.classList.remove("close");
    menu.classList.remove("show");
    menuNav.classList.remove("show");
    menuBrand.classList.remove("show");
    navItems.forEach((element) => {
      element.classList.remove("show");
    });

    // set menu state
    showMenu = false;
  }
}

document.addEventListener("DOMContentLoaded", () => {
  // Get theme value from session storage
  let theme = sessionStorage.getItem("theme");

  // Set theme value
  document.documentElement.setAttribute("data-theme", theme || "light");

  // Get switcher element
  var themeSwitcher = document.querySelector('input[type="checkbox"]');

  // If current theme is dark then switcher is checked
  if (theme && theme === "dark") themeSwitcher.checked = true;

  // On switcher change set theme
  themeSwitcher.addEventListener("change", () => {
    if (themeSwitcher.checked) {
      // If checked then set dark theme
      document.documentElement.setAttribute("data-theme", "dark");
      sessionStorage.setItem("theme", "dark");
    } else {
      // else set light theme
      document.documentElement.setAttribute("data-theme", "light");
      sessionStorage.setItem("theme", "ligth");
    }
  });
});

// Form

function onSubmit() {
  const valid = validateForm();
  if (valid == true) {
    alert("The message was submitted");
  } else {
    return false;
  }
}

function validateForm() {
  const name = document.forms["myForm"]["fName"].value;
  const email = document.forms["myForm"]["fEmail"].value;
  const message = document.forms["myForm"]["mes"].value;
  if (name == "") {
    alert("Name must be filled out");
    return false;
  }

  if (email == "") {
    alert("Email must be filled out");
    return false;
  }
  if (!email.includes("@")) {
    alert("Email must include @");
    return false;
  }

  if (message == "") {
    alert("Message must be filled out");
    return false;
  }

  return true;
}
