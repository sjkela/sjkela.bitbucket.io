# README #


### What is this? ###

LUT university Software Development Skills: Front-End 2020-21 course project.


### How do I run this app? ###

* Go to https://sjkela.bitbucket.io/
* or
* Go in root index.html and run it in live server

* If you want to run course task app sass script then run in commad line:
* $ git run sass-tasks

* If you want to run main course project sass script then run in comman line:
* $ git run sass-project


### Who do I talk to? ###

* Juulia Kela
* juulia.kela@gmail.com or Juulia.Kela@student.lut.fi